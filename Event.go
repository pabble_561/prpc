package prpc

import "reflect"

type C map[interface{}]interface{}
type function func(interface{})

type entity struct {
	evtName string
	target  interface{}
	cb      function
}

type Event struct {
	events []entity
}

func NewEvent() *Event {
	var evt = &Event{}
	evt.events = make([]entity, 0)
	return evt
}

func (e *Event) On(evtName string, fc function, target interface{}) {
	var ent = entity{evtName: evtName, cb: fc, target: target}
	e.events = append(e.events, ent)
}

func (e *Event) Emit(evtName string, args interface{}) int {
	var count = 0
	for _, v := range e.events {
		if v.evtName == evtName {
			v.cb(args)
			count++
		}
	}
	return count
}
func (e *Event) Off(val interface{}) int {
	var arr []int
	switch value := val.(type) {
	case string:
		for idx, v := range e.events {
			if v.evtName == val {
				arr = append(arr, idx)
			}
		}
		_ = value
		break
	default:
		for idx, v := range e.events {
			if reflect.DeepEqual(v.target, val) {
				arr = append(arr, idx)
			}
		}
		break
	}
	var le = len(arr)
	for i := le - 1; i >= 0; i-- {
		var idx = arr[i]
		e.events = append(e.events[:idx], e.events[(idx+1):]...)
	}
	return le
}
