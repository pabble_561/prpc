package prpc

type ServerCfg struct {
	Port int
}

type RPCServer struct {
	server   *TcpSocketServer
	IsServer bool
	port     int
	Event    *Event
}

func CreateServer(port int) *RPCServer {
	var sev = &RPCServer{Event: NewEvent()}
	sev.IsServer = false
	sev.server = CreateSocketServer("0.0.0.0", port)
	sev.port = port
	sev.listen()
	return sev
}

func (r *RPCServer) listen() {
	r.server.On(SOCKET_NEW_AGENT, r.onNewAgent, r)
	r.server.On(SOCKET_DISCONNECT, r.onDisconnect, r)
	r.server.On(SOCKET_MESSAGE, r.onMessage, r)
}

func (r *RPCServer) onNewAgent(data interface{}) {
	r.Event.Emit(SOCKET_NEW_AGENT, data)
}

func (r *RPCServer) onDisconnect(data interface{}) {
	r.Event.Emit(SOCKET_DISCONNECT, data)
}

func (r *RPCServer) onMessage(data interface{}) {
	r.Event.Emit(SOCKET_MESSAGE, data)
}
