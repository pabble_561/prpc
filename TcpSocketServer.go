package prpc

import (
	"fmt"
	"net"
	"time"
)

var SOCKET_NEW_AGENT = "socket_newAgent"
var SOCKET_DISCONNECT = "socket_disconnet"
var SOCKET_MESSAGE = "socket_message"

type TcpSocketServer struct {
	msgChann  chan []byte
	isConnect bool
	Conn      net.Conn
	Event
}

var exByte = []byte("0")
var headBeatByte = []byte("1")

func CreateSocketServer(host string, port int) *TcpSocketServer {
	var url = fmt.Sprintf("%s:%d", host, port)
	listener, err := net.Listen("tcp", url)
	if err != nil {
		panic(err)
	}

	var tcp = &TcpSocketServer{isConnect: false, msgChann: make(chan []byte)}
	go tcp.listen(listener)
	return tcp
}
func (tcp *TcpSocketServer) disconnect() {
	if tcp.isConnect {
		tcp.isConnect = false
		tcp.Conn.Close()
	}
}

func (tcp *TcpSocketServer) listen(listener net.Listener) {

	for {
		conn, e := listener.Accept()
		if e != nil {
			continue
		}
		tcp.isConnect = true
		tcp.Conn = conn
		tcp.Emit(SOCKET_NEW_AGENT, tcp)
		go tcp.read(conn)
		go tcp.write(conn)
	}
}
func (tcp *TcpSocketServer) read(conn net.Conn) {
	conn.SetReadDeadline(time.Now().Add(10 * time.Second))
	defer func() {
		conn.Close()
	}()
	buffer := make([]byte, 1024)
	for {
		n, err := conn.Read(buffer)
		if err != nil {
			//tcp.msgChann <- exByte
			return
		}
		//clientMsg := string(buffer[0:n])
		conn.SetReadDeadline(time.Now().Add(10 * time.Second))
		tcp.Emit(SOCKET_MESSAGE, buffer[0:n])
	}
}

func (tcp *TcpSocketServer) write(conn net.Conn) {
	defer func() {
		tcp.Emit(SOCKET_DISCONNECT, tcp)
	}()
	for {
		message, ok := <-tcp.msgChann
		if !ok {
			tcp.disconnect()
			break
		}
		if len(message) == 1 && message[0] == exByte[0] {
			tcp.disconnect()
			break
		}
		fmt.Println("写入消息:", message)
		conn.Write(message)
	}
}

func (tcp *TcpSocketServer) Send(msg []byte) {
	tcp.msgChann <- msg
}
