package prpc

import (
	"encoding/binary"
	_ "fmt"
	_ "time"
)

type rpc struct {
	IsServer  bool
	isConnect bool
}

type RPCClient struct {
	Socket        *TcpSocketClient
	cfg           ClientCfg
	ReConnectTime int64
	rpc
	Event *Event
}

type ClientCfg struct {
	RemoteHost    string
	Port          int
	ReConnectTime int64
}

func CreateRPCClient(cfgs ClientCfg) *RPCClient {
	var client = &RPCClient{Event: NewEvent()}
	client.IsServer = false
	client.isConnect = false
	client.Socket = NewClient(cfgs)
	client.listen(client.Socket)
	client.cfg = cfgs
	return client
}

func (c *RPCClient) Connect() {
	c.Socket.Connect()
	c.isConnect = true
}

func (c *RPCClient) SendHeadBeat() {
	c.Socket.Send(headBeatByte)
}

func (c *RPCClient) Send(msg []byte) {
	m := make([]byte, 2+len(msg))
	binary.BigEndian.PutUint16(m, uint16(len(msg)))
	copy(m[2:], msg)
	c.Socket.Send(m)
}

func (r *RPCClient) listen(client *TcpSocketClient) {
	client.On(SOCKET_NEW_AGENT, r.onNewAgent, r)
	client.On(SOCKET_DISCONNECT, r.onDisconnect, r)
	client.On(SOCKET_MESSAGE, r.onMessage, r)
}

func (r *RPCClient) onNewAgent(data interface{}) {
	r.Event.Emit(SOCKET_NEW_AGENT, data)
}

func (r *RPCClient) onDisconnect(data interface{}) {
	r.Event.Emit(SOCKET_DISCONNECT, data)
}

func (r *RPCClient) onMessage(data interface{}) {
	r.Event.Emit(SOCKET_MESSAGE, data)
}
