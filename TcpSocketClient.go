package prpc

import (
	"fmt"
	"net"
	"time"
)

type TcpSocketClient struct {
	Message   chan []byte //输出消息
	Config    ClientCfg
	IsConnect bool

	msgChann      chan []byte
	connectParams string
	Conn          net.Conn
	Event
}

func NewClient(cfg ClientCfg) *TcpSocketClient {
	var tcp = &TcpSocketClient{IsConnect: false, Config: cfg, msgChann: make(chan []byte)}
	tcp.connectParams = fmt.Sprintf("%s:%d", cfg.RemoteHost, cfg.Port)
	return tcp
}

func (tcp *TcpSocketClient) Connect() bool {
	if tcp.IsConnect {
		return true
	}
	conn, err := net.Dial("tcp", tcp.connectParams)
	if err != nil {
		return false
	}
	tcp.IsConnect = true
	tcp.Conn = conn
	tcp.Emit(SOCKET_NEW_AGENT, tcp)
	go tcp.read()
	go tcp.write()
	return true
}

func (tcp *TcpSocketClient) read() {
	tcp.Conn.SetReadDeadline(time.Now().Add(5 * time.Second))
	//准备命令行标准输入
	buffer := make([]byte, 1024)
	for {
		n, err := tcp.Conn.Read(buffer)
		if err != nil {
			tcp.msgChann <- exByte
			break
		}
		//clientMsg := string(buffer[0:n])
		tcp.Conn.SetReadDeadline(time.Now().Add(5 * time.Second))
		tcp.Message <- buffer[0:n]
		tcp.Emit(SOCKET_MESSAGE, tcp)
	}
}

func (tcp *TcpSocketClient) write() {
	defer func() {
		tcp.Emit(SOCKET_DISCONNECT, tcp)
	}()
	for {
		message, ok := <-tcp.msgChann
		if !ok {
			tcp.disconnect()
			break
		}
		if len(message) == 1 && message[0] == 0 {
			tcp.disconnect()
			break
		}
		_, e := tcp.Conn.Write(message)
		if e != nil {
			tcp.disconnect()
			break
		}
	}
}

func (tcp *TcpSocketClient) disconnect() {
	if tcp.IsConnect {
		tcp.IsConnect = false
		tcp.Conn.Close()
	}
}

func (tcp *TcpSocketClient) Send(msg []byte) {
	tcp.msgChann <- msg
}
